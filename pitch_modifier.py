import bpy
import operator
from . import vanotesgrabber
from . import vapart


class STRIP_OT_change_pitch_hard(bpy.types.Operator):
    bl_label = "Pitch modify hard"
    bl_idname = "strip.change_pitch_hard"
    bl_description = "Set pitch to all sound strips volume as octave offset"

    note_spawner = None

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) > 0

    @classmethod
    def do_work_recursively(cls, context, notes_spawner, sequences, octave_offset):#RP_TODO сделать возможность заменять также видео
        def deselect_recursively(sequences):
            for sequence in sequences:
                if sequence.type == 'SOUND':
                    sequence.select = False
                elif sequence.type == 'META':
                    deselect_recursively(sequence.sequences)

        #alternative deselect_recursively(sequences)
        bpy.ops.sequencer.select_all(action='DESELECT')

        for sequence in list(sequences):
            if sequence.type == 'SOUND':
                if '.' in sequence.name:
                    old_note_name = sequence.name.split('.')[0]
                else:
                    old_note_name = sequence.name

                #print ('OLD NAME: ' + old_note_name)
                new_note_name = vapart.VAPart.make_pitched_note_name(old_note_name,
                                                                     int(old_note_name[-1]) + octave_offset)

                frame_start = sequence.frame_final_start
                sequence_length_in_frames = sequence.frame_final_duration
                channel = sequence.channel

                sequence.select = True
                bpy.ops.sequencer.delete()

                #print('FS' + str(frame_start))
                #print('FD' + str(sequence_length_in_frames))
                m,v = notes_spawner.spawnnote(new_note_name, frame_start, sequence_length_in_frames, 31, 32,
                                        context.scene.sequence_editor, use_video=False,
                                        try_pitching_for_missing_notes=True)

                m.channel=channel
                m.select = False
            elif sequence.type == 'META':
                cls.do_work_recursively(context,notes_spawner, sequence.sequences, octave_offset)

    def execute(self, context):
        notes_dir = context.scene.vapart_notedatadir
        note_audio_offset = context.scene.vapart_properties.note_audio_start_offset

        notes_data = vanotesgrabber.get_notes_from_files_in_dir(notes_dir, note_audio_offset, is_drum=False)
        note_spawner = vapart.VAPart(notes_data)  # RP_TODO make method setting this from outer, paste checks in poll

        self.do_work_recursively(context, note_spawner, context.selected_sequences,
                                 context.scene.vapart_properties.octave_offset_hard)
        return {'FINISHED'}
