import bpy
from operator import attrgetter

half_frame_width = 50

class StripPosKey:

    def __init__(self, pos_x, pos_y, scale=1.0, matrix=None):
        mw,mh = 0,0
        if matrix is not None:
            mw, mh = matrix.split('x')

        self.matrix_w = int(mw)
        self.matrix_h = int(mh)
        self.pos_x = int(pos_x)
        self.pos_y = int(pos_y)
        self.scale = float(scale)

    @staticmethod
    def get_frame_pos_from_matrix_pos(mtrx_x, mtrx_y, half_frame_width):
        max_param = max(abs(mtrx_x), abs(mtrx_y))
        increment = half_frame_width/(1+max_param)
        preferred_scale = 1/(1 + max_param)

        return mtrx_x * increment, mtrx_y * increment, preferred_scale


def get_transform_effect_at_frame(sequence_editor, frame=-1, strip=None):
    for s in sequence_editor.sequences_all:

        if s.type == 'TRANSFORM':
            if strip is not None:
                if strip.type == 'TRANSFORM':
                    strip = strip.input_1

                if strip == s.input_1:
                    return s
            elif s.type == 'TRANSFORM' and (s.input_1.frame_final_start <= frame <= s.input_1.frame_final_end):
                return s

    return None


def set_strip_position(strip, pos_key=None, strait_x=0.0, strait_y=0.0, strait_scale=1.0, channel=-1):
    tr_strip = strip

    if strip.type != 'TRANSFORM':
        tr_strip = get_transform_effect_at_frame(bpy.context.scene.sequence_editor, strip=strip)
        if tr_strip is None:
            channel = (strip.channel + 1) if channel == -1 else channel
            channel %= 32
            tr_strip = bpy.context.scene.sequence_editor.sequences.new_effect('transform' + strip.name, 'TRANSFORM',
                                                                              channel,
                                                                              frame_start = strip.frame_final_start,
                                                                              frame_end= strip.frame_final_end,
                                                                              seq1=strip)

    if pos_key is None:
        px, py, scale = strait_x, strait_y, strait_scale
    else:
        px, py, scale = StripPosKey.get_frame_pos_from_matrix_pos(pos_key.pos_x, pos_key.pos_y, half_frame_width)
        scale = pos_key.scale

    tr_strip.scale_start_x = tr_strip.scale_start_y = scale
    tr_strip.translate_start_x = px
    tr_strip.translate_start_y = py

    tr_strip.blend_type = 'OVER_DROP'


def get_strips_between(context, strip1, strip2, strip_type={'MOVIE'}, inclusive=True):
    se = context.scene.sequence_editor

    current_sequences = None
    if len(se.meta_stack) == 0:
        current_sequences = se.sequences #top level
    else:
        current_sequences = se.meta_stack[-1].sequences


    if strip1.type == 'TRANSFORM':
        strip1 = strip1.input_1

    if strip2.type == 'TRANSFORM':
        strip2 = strip2.input_1


    def is_strip_fits_conditions(strip, strip1, strip2, strip_type, inclusive):
        fits_type = strip.type in strip_type
        if fits_type:
            if inclusive:
                return strip1.frame_final_start <= strip.frame_final_start <= strip2.frame_final_end
            else:
                return strip1.frame_final_start < strip.frame_final_start < strip2.frame_final_start

        return False

    current_sequences = [s for s in current_sequences if is_strip_fits_conditions(s, strip1, strip2, strip_type, inclusive)]

    return current_sequences


class VAPartSetStripTransformPosition(bpy.types.Operator):
    bl_label = "Set transform pos"
    bl_idname = "vapart.set_strip_transform_position"
    bl_description = "Set transform to selected strips"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) > 0

    def execute(self, context):
        strip_pos_y = context.scene.vapart_properties.strip_pos_y
        strip_pos_x = context.scene.vapart_properties.strip_pos_x
        matrix = context.scene.vapart_properties.video_matrix
        scale = context.scene.vapart_properties.strip_scale

        pos_key = StripPosKey(strip_pos_x, strip_pos_y, scale)
        for s in context.selected_sequences:
            if s.type == 'TRANSFORM':
                s = s.input_1

            set_strip_position(strip=s, pos_key=pos_key)
            #res = get_transform_effect_at_frame(context.scene.sequence_editor, strip=s) # int((s.frame_final_start + s.frame_final_end)*0.5)) #set_strip_position(None, s)
            #print(res)

        return {'FINISHED'}


class VAPartInterpolateStripTransformPositions(bpy.types.Operator):
    bl_label = "Interpolate strip pos"
    bl_idname = "vapart.interpolate_strip_transform_positions"
    bl_description = "Interpolate strip transform positions"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) >= 2

    @staticmethod
    def interpolate(v1,v2, t):
        return v1*(1 - t) + v2*t

    def execute(self, context):
        def key_func(strip):
            if strip.type == 'TRANSFORM':
                strip = strip.input_1
            return strip.frame_final_start

        # delete video_strips = [vs for vs in context.selected_sequences if vs.type == 'MOVIE']
        sorted_strips = sorted(context.selected_sequences, key=key_func)
        strip1 = sorted_strips[0]
        strip2 = sorted_strips[-1]
        strips_between = sorted(get_strips_between(context, strip1, strip2, strip_type={'MOVIE'}, inclusive=False),
                                key=key_func)

        tr_1 = get_transform_effect_at_frame(context.scene.sequence_editor, strip=strip1)
        tr_2 = get_transform_effect_at_frame(context.scene.sequence_editor, strip=strip2)

        for i, s in enumerate(strips_between):
            #delete она и так должна быть трансформ if s.type == 'TRANSFORM':
            #     s = s.input_1

            t = float(i + 1) / (len(strips_between) + 1)
            strip_pos_x = self.interpolate(tr_1.translate_start_x, tr_2.translate_start_x, t)
            strip_pos_y = self.interpolate(tr_1.translate_start_y, tr_2.translate_start_y, t)
            strip_scale = self.interpolate(tr_1.scale_start_x, tr_2.scale_start_x, t)
            #pos_key = StripPosKey(strip_pos_x, strip_pos_y, strip_scale)
            set_strip_position(s, strait_x=strip_pos_x, strait_y=strip_pos_y, strait_scale=strip_scale)

        return {'FINISHED'}


def find_strips_of_same_start_and_duration_and_type(strips, types=None):
    # def key_func(strip):
    #     if strip.type == 'TRANSFORM':
    #         strip = strip.input_1
    #     return strip.frame_final_start

    strips_of_type = strips

    if types is not None:
        strips_of_type = [vs for vs in strips if vs.type in types]

    strips_of_type = sorted(strips_of_type, key=attrgetter('frame_final_start', 'frame_final_duration', 'channel'))

    chords = []
    for s in strips_of_type:
        if len(chords) == 0 or chords[-1][-1].frame_final_start != s.frame_final_start or chords[-1][-1].frame_final_duration != s.frame_final_duration:
            chords.append([s])
        else:
            chords[-1].append(s)

    return chords


class VAPartPositionChordStripsSquared(bpy.types.Operator):
    bl_label = "Chordify strips"
    bl_idname = "vapart.make_chord_strips_squared"
    bl_description = "Makes chord strips squared"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) >= 2

    def execute(self, context):
        chords = find_strips_of_same_start_and_duration_and_type(context.selected_sequences, ('MOVIE'))

        frame_square_indexed = ((-1, 1), (1, 1), (1, -1), (-1, -1))
        for chord_strips in chords:
            count = len(chord_strips)
            if count == 1:
                s = chord_strips[0]
                set_strip_position(s, channel=s.channel + count)
            else:
                for i, s in enumerate(chord_strips):
                    pos = frame_square_indexed[i%4]
                    pos_key = StripPosKey(pos[0], pos[1], .5)
                    set_strip_position(s, pos_key=pos_key, channel=s.channel+count)

        return {'FINISHED'}


class VAPartPositionChordStripsInCorners(bpy.types.Operator):
    bl_label = "Chordify strips"
    bl_idname = "vapart.make_chord_strips_cornered"
    bl_description = "Makes chord strips cornered"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) >= 2

    def execute(self, context):
        chords = find_strips_of_same_start_and_duration_and_type(context.selected_sequences, ('MOVIE'))

        frame_square_indexed = ((-2, 2), (2, 2), (2, -2), (-2, -2))
        for chord_strips in chords:
            count = len(chord_strips)
            if count == 1:
                s = chord_strips[0]
                set_strip_position(s, channel=s.channel + count)
            else:
                for i, s in enumerate(chord_strips):
                    if i == 0:
                        set_strip_position(s, strait_x=0, strait_y=0, strait_scale=1, channel=s.channel + count)
                    else:
                        pos = frame_square_indexed[(i - 1) % 4]
                        pos_key = StripPosKey(pos[0], pos[1], .25)
                        set_strip_position(s, pos_key=pos_key, channel=s.channel+count)

        return {'FINISHED'}


class VAPartFixStripsGaps(bpy.types.Operator):
    bl_label = "Fill gaps"
    bl_idname = "vapart.fix_strips_gaps"
    bl_description = "Fills the gaps by previous strips"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) >= 2

    def execute(self, context):
        chords = find_strips_of_same_start_and_duration_and_type(context.selected_sequences, ('MOVIE', 'SOUND'))

        chords_len = len(chords)
        if chords_len < 2:
            return {'WARNING'}

        cur_strip_index = 0
        next_strip_index = 1

        while next_strip_index < chords_len:
            cur_strips = chords[cur_strip_index]
            next_strips = chords[next_strip_index]
            if cur_strips[0].frame_final_start == next_strips[0].frame_final_start:
                next_strip_index += 1
                cur_strip_index = next_strip_index - 1
            else:
                if cur_strips[0].frame_final_end > next_strips[0].frame_final_end:
                    next_strip_index += 1
                elif cur_strips[0].frame_final_end >= next_strips[0].frame_final_start:
                    cur_strip_index = next_strip_index
                    next_strip_index = cur_strip_index + 1
                else:
                    for s in cur_strips:
                        s.frame_final_end = next_strips[0].frame_final_start

                    cur_strip_index = next_strip_index
                    next_strip_index += 1


        return {'FINISHED'}


class VAPartLetSoundStripsRing(bpy.types.Operator):
    bl_label = "DRAFT let ring"
    bl_idname = "vapart.let_ring"
    bl_description = "some draft stuff"

    @classmethod
    def poll(cls, context):
        return len(context.selected_sequences) > 0

    def execute(self, context):
        k = 1.2
        #chords = find_strips_of_same_start_and_duration_and_type(context.selected_sequences, 'MOVIE')

        for s in context.selected_sequences:
            if s.type == 'SOUND':
                s.frame_final_duration *= k

        return {'FINISHED'}