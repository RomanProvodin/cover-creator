
bl_info = {
    'name': 'covercreator',
    'author': 'Roman Provodin aka plutonosvet',
    'location': 'View3D > UI panel > Add meshes',
    'category': '3D View'
}

if "bpy" in locals():
    import importlib

    importlib.reload(vapart)
    importlib.reload(vanotesgrabber)
    importlib.reload(volume_modifier)
    importlib.reload(pitch_modifier)
    importlib.reload(va_helpers)
    importlib.reload(va_video_positioning)
else:
    import bpy
    import music21
    import os
    from . import (
        vapart,
        vanotesgrabber,
        volume_modifier,
        pitch_modifier,
        va_helpers,
        va_video_positioning
    )



parsed_midi_file = None

def midi_file_path_updated(self,context):
    #print('TRYING to parse: ' + context.scene.vapart_midifilepath)
    global parsed_midi_file
    parsed_midi_file = music21.converter.parse(bpy.context.scene.vapart_midifilepath)
    #print(parsed_midi_file)


#midi_file_path_updated(None, bpy.context)


def main_function(midi_filepath, notes_folder, track_index, use_video=True, notes_audio_start_offset=0, octave_offset=0, tempo=1.0, is_drum=False):
    notesdata = vanotesgrabber.get_notes_from_files_in_dir(notes_folder, notes_audio_start_offset, is_drum=is_drum)

    bpy.context.scene.sequence_editor_create()

    c = music21.converter.parse(midi_filepath)

    parsed_midi_file = c

    p0 = c.parts[track_index].flat.elements

    mm = c.metronomeMarkBoundaries()[0][2]

    partNotes = vapart.VAPart(notesdata)

    FPS = bpy.context.scene.render.fps / bpy.context.scene.render.fps_base



    curFrame = bpy.context.scene.frame_current
    length_tempo = tempo - 0

    layer_max = 28
    audio_ch = 1
    video_ch = layer_max//2 + 1
    for p in p0:
        curNoteToSpawnPos = mm.durationToSeconds(p.offset) * FPS / tempo
        if curNoteToSpawnPos> curFrame:
            break

        if isinstance(p, music21.note.Note):

            if audio_ch > layer_max//2:
                audio_ch = 1

            if video_ch > layer_max:
                video_ch = layer_max // 2 + 1

            # print (p.pitch.nameWithOctave)

            if is_drum:
                nwo = str(p.pitch.midi)
                #print('DRUM: ' + nwo)
            else:
                nwo = p.pitch.nameWithOctave
                nwo = nwo[:-1]+ str(int(nwo[-1]) + octave_offset)
            #print(nwo,end='')
            mv = partNotes.spawnnote(nwo,#p.pitch.nameWithOctave,
                                     mm.durationToSeconds(p.offset) * FPS/ tempo,
                                     mm.durationToSeconds(p.duration.quarterLength) * FPS/ length_tempo,
                                     31,
                                     32,
                                     bpy.context.scene.sequence_editor,
                                     use_video,
                                     try_pitching_for_missing_notes=True,
                                     is_drum=is_drum)

            if mv == None:
                if is_drum:
                    print ('MISSING DRUM: ' + nwo)
                else:
                    print('FATAL')
                continue
            else:
                pass #print('')

            mv[0].channel = audio_ch
            audio_ch += 1

            if mv[1] is not None:
                mv[1].channel = video_ch

            video_ch += 1

        elif isinstance(p, music21.chord.Chord):
            chord_offset_frames = 0# to make them brrrriiin'
            pasted_chord_notes = []
            for pitch in p.pitches:

                if audio_ch > layer_max // 2:
                    audio_ch = 1

                if video_ch > layer_max:
                    video_ch = layer_max // 2 + 1

                # noteInChord = note.Note(nameWithOctave=pitch.unicodeNameWithOctave,
                #                         quarterLength=p.duration.quarterLength)

                if is_drum:
                    nwo = str(pitch.midi)
                    #print('DRUM: ' + nwo, end=' ')
                else:
                    nwo = pitch.nameWithOctave
                    nwo = nwo[:-1] + str(int(nwo[-1]) + octave_offset)
                #print(nwo, end=' ')

                #makes notes in chord to be unique
                if nwo in pasted_chord_notes:
                    continue

                mv = partNotes.spawnnote(nwo,
                                         mm.durationToSeconds(p.offset) * FPS / tempo + chord_offset_frames,
                                         mm.durationToSeconds(p.duration.quarterLength) * FPS / length_tempo,
                                         31,
                                         32,
                                         bpy.context.scene.sequence_editor,
                                         use_video,
                                         try_pitching_for_missing_notes=True,
                                         is_drum=is_drum)

                #chord_offset_frames += 1
                pasted_chord_notes.append(nwo)

                if mv is None:
                    if is_drum:
                        print('MISSING DRUM: ' + nwo)
                    else:
                        print('FATAL')
                    continue
                else:
                    pass #print('')

                mv[0].channel = audio_ch
                audio_ch += 1

                if mv[1] is not None:
                    mv[1].channel = video_ch

                video_ch += 1
        else:
            print('unknown')

####################################################
#        soundPathToNoteTmp = notesAndPaths[p.pitch.unicodeName]
#        paste_note(
#            soundPathToNoteTmp,#vidPath,
#            p,
#            mm.durationToSeconds(p.offset)*FPS,
#            mm.durationToSeconds( p.duration.quarterLength),
#            ch,
#            bpy.context.scene.sequence_editor)
# print('test')



# ch+=1
# print('Chord\t%s\t%s' % (p.offset, p.duration.quarterLength))

# m = bpy.context.scene.sequence_editor.sequences.new_sound(
#                name='TESTs',
#                filepath= r'F:\kuzhez\t1.mp3',
#                channel=1, frame_start=50)


class VACoverCreatorOperator(bpy.types.Operator):
    bl_label = "Create Cover"
    bl_idname = "sequence_editor.cover_creator_create"
    bl_description = "Creates a cover by midi and your notes"

    @classmethod
    def poll(cls, context):
        midi_path = context.scene.vapart_midifilepath
        notes_dir_path = context.scene.vapart_notedatadir

        return len(notes_dir_path) > 0 and len(midi_path) >= 5 and midi_path[-4:] == '.mid'

    def execute(self, context):
        midi_path = context.scene.vapart_midifilepath
        notes_dir_path = context.scene.vapart_notedatadir
        track_index = int(context.scene.vapart_properties.track_index)
        note_audio_offset = context.scene.vapart_properties.note_audio_start_offset
        octave_offset = context.scene.vapart_properties.octave_offset
        tempo = context.scene.vapart_properties.tempo
        is_drum = context.scene.vapart_properties.is_drum_track

        main_function(midi_path, notes_dir_path, track_index, use_video=True,
                      notes_audio_start_offset=note_audio_offset, octave_offset=octave_offset, tempo=tempo, is_drum=is_drum)
        # print(locals())
        # self.report({'INFO'}, "Moving the ball")
        return {'FINISHED'}


class VACoverCreatorReloadMidiOperator(bpy.types.Operator):
    bl_label = "Reload midi"
    bl_idname = "sequence_editor.cover_creator_reload_midi"
    bl_description = "Reloads midi file to update track list"

    @classmethod
    def poll(cls, context):
        midi_path = context.scene.vapart_midifilepath
        notes_dir_path = context.scene.vapart_notedatadir

        return len(notes_dir_path) > 0 and len(midi_path) >= 5 and midi_path[-4:] == '.mid'

    def execute(self, context):
        midi_file_path_updated(self, context)
        return {'FINISHED'}



#delete  class VATrackIndexSelector(bpy.types.Operator):
#     bl_idname = "vapart.select_track_index"
#     bl_label = "Track index"
#
#     # def draw(self,context):
#     #     layout = self.layout
#     #     layout.prop(self, 'track_index')
#
#     def execute(self,context):
#         print(context.scene.vapart_properties.track_index)
#         return {'FINISHED'}


def get_track_data(self, context):
    if parsed_midi_file is not None:
        parts = parsed_midi_file.parts
        res = []

        #print('TRYING to fill enum data')
        for i, part in enumerate(parts):
            new_tuple = (str(i), str(i) + ' ' + (part.partName if part.partName is not None else 'None'), '')
            res.append(new_tuple)
            #print('ADDING ' + str(new_tuple))
        return res

    #print('FAILED to fetch tracks data from: ' + str(parsed_midi_file))
    return []


def handle_on_strip_pos_changed(self, context):
    data = va_video_positioning.StripPosKey.get_frame_pos_from_matrix_pos(self.strip_pos_x, self.strip_pos_y, va_video_positioning.half_frame_width)
    self.strip_scale = data[2]


class VAPartitionProperties(bpy.types.PropertyGroup):
    track_index = bpy.props.EnumProperty(items=get_track_data,
                                         name="track index")

    is_drum_track = bpy.props.BoolProperty(name="Is drum track")

    note_audio_start_offset = bpy.props.IntProperty(name="Note audio offset")

    octave_offset = bpy.props.IntProperty(name="Octave offset")
    octave_offset_hard = bpy.props.IntProperty(name="Octave offset hard")

    tempo = bpy.props.FloatProperty(name="Tempo", default=1.0)

################################################
    # VIDEO POSING
    video_matrix = bpy.props.EnumProperty(items=(('1x1', '1x1', '1x1'),
                                                 ('2x2', '2x2', '2x2'),
                                                 ('3x3', '3x3', '3x3'),
                                                 ('4x4', '4x4', '4x4'),
                                                 ('5x5', '5x5', '5x5')),
                                          name="video matrix")

    strip_pos_x = bpy.props.IntProperty(name="X", update=handle_on_strip_pos_changed)
    strip_pos_y = bpy.props.IntProperty(name="Y", update=handle_on_strip_pos_changed)
    strip_scale = bpy.props.FloatProperty(name="Scale")
################################################
    # primitive = bpy.props.EnumProperty(
    #     items=mode_options,
    #     description="offers....",
    #     default="mesh.primitive_plane_add",
    #     update=execute_operator
    # )
    pass


class OBJECT_PT_pingpong(bpy.types.Panel):
    bl_label = "Cover creator"
    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "UI"

    # bl_category = 'ShortCuts :-)'
    # bl_context = "object"

    # filepath = bpy.props.StringProperty(subtype="FILE_PATH")

    def draw_header(self, context):
        self.layout.label(text="", icon="PHYSICS")

    def draw(self, context):
        row = self.layout.row()
        row.operator("sequence_editor.cover_creator_create")

        # row = self.layout.row()
        # row.operator(va_helpers.VAHELPER_OT_meta_from_selected_audios.bl_idname)

        # row = self.layout.row()
        # row.operator(va_helpers.VAHELPER_OT_set_proxies_to_all_selected_submovies.bl_idname)

        row = self.layout.row()
        row.operator(VACoverCreatorReloadMidiOperator.bl_idname)
        row = self.layout.row()
        row.prop(context.scene.vapart_properties, 'track_index')

        row = self.layout.row()
        row.prop(context.scene.vapart_properties, 'is_drum_track')

        row = self.layout.row()
        row.operator("strip.collectmarkernotes")

        row = self.layout.row()
        row.operator("strip.setnotemarkerlength")

        row = self.layout.row()
        row.prop(context.scene, 'vapart_midifilepath', text='Midi file:')

#####################################
        new_layout = self.layout.box()
        row = new_layout.row()
        row.prop(context.scene, 'vapart_notedatadir', text='Notes folder:')

        row = new_layout.row()
        row.prop(context.scene.vapart_properties, 'note_audio_start_offset')
        # row.operator("vapartition.select_midi", icon="FILE_FOLDER", text="")

        row = new_layout.row()
        row.prop(context.scene.vapart_properties, 'octave_offset')

        row = new_layout.row()
        row.prop(context.scene.vapart_properties, 'tempo')
#####################################

        new_layout = self.layout.box()
        new_layout.label ('from notes to strips')
        row = new_layout.row()
        row.prop(context.scene, 'vapart_notedatafile', text='')
        row = new_layout.row()
        row.operator('strip.spawnmarkernotes_and_strips')

#####################################
        # VIDEO POSING
        new_layout = self.layout.box()
        row = new_layout.row()
        row.prop(context.scene.vapart_properties, 'video_matrix')

        row = new_layout.row()
        row.prop(context.scene.vapart_properties, 'strip_pos_x')

        row = new_layout.row()
        row.prop(context.scene.vapart_properties, 'strip_pos_y')

        row = new_layout.row()
        row.prop(context.scene.vapart_properties, 'strip_scale')

        row = new_layout.row()
        row.operator(va_video_positioning.VAPartSetStripTransformPosition.bl_idname)

        row = new_layout.row()
        row.operator(va_video_positioning.VAPartInterpolateStripTransformPositions.bl_idname)

        row = new_layout.row()
        row.operator(va_video_positioning.VAPartPositionChordStripsSquared.bl_idname)

        row = new_layout.row()
        row.operator(va_video_positioning.VAPartPositionChordStripsInCorners.bl_idname)

        row = new_layout.row()
        row.operator(va_video_positioning.VAPartFixStripsGaps.bl_idname)

        row = new_layout.row()
        row.operator(va_video_positioning.VAPartLetSoundStripsRing.bl_idname)

#####################################


        new_layout = self.layout.box()
        new_layout.label('Volume changer')

        row = new_layout.row()
        row.prop(context.scene, 'vapart_volume_modify_add', text='')
        row = new_layout.row()
        row.operator('strip.change_volume_add')

        row = new_layout.row()
        row.prop(context.scene, 'vapart_volume_modify_multiply', text='')
        row = new_layout.row()
        row.operator('strip.change_volume_multiply')

        row = new_layout.row()
        row.prop(context.scene, 'vapart_volume_modify_indent', text='')
        row = new_layout.row()
        row.operator(volume_modifier.STRIP_OT_change_volume_indent.bl_idname)

        row = new_layout.row()
        row.operator(va_helpers.VAHELPER_OT_select_sound_chord_strips.bl_idname)



##################################
        new_layout = self.layout.box()
        new_layout.label('Pitch changer')

        row = new_layout.row()
        row.prop(context.scene.vapart_properties, 'octave_offset_hard')
        row = new_layout.row()
        row.operator(pitch_modifier.STRIP_OT_change_pitch_hard.bl_idname)

##################################


        #row = new_layout.row()
        #row.operator('vapart.select_track_index')#row.prop(context.scene,'vapart_track_index_to_generate')






def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.vapart_midifilepath = bpy.props.StringProperty(subtype='FILE_PATH', update=midi_file_path_updated)
    bpy.types.Scene.vapart_notedatadir = bpy.props.StringProperty(subtype='DIR_PATH')
    bpy.types.Scene.vapart_notedatafile = bpy.props.StringProperty(description="note data to generate strips and markers", subtype='FILE_PATH')
    bpy.types.Scene.vapart_volume_modify_add = bpy.props.FloatProperty(description="additive volume change")
    bpy.types.Scene.vapart_volume_modify_multiply = bpy.props.FloatProperty(description="multipling volume change")
    bpy.types.Scene.vapart_volume_modify_indent = bpy.props.FloatProperty(description="ind volume change")

    bpy.types.Scene.vapart_properties = bpy.props.PointerProperty(type=VAPartitionProperties)


def unregister():
    bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.vapart_midifilepath
    del bpy.types.Scene.vapart_notedatadir
    del bpy.types.Scene.vapart_notedatafile
    del bpy.types.Scene.vapart_volume_modify_add
    del bpy.types.Scene.vapart_volume_modify_multiply
    del bpy.types.Scene.vapart_volume_modify_indent

    del bpy.types.Scene.vapart_properties


if __name__ == "__main__":
    register()
    #midi_file_path_updated(None, bpy.context)

# сделать чтобы через интерфейс менять базовый отступ начала нот
# сделать чтобы мета стрипы создавались с названием трека в миди
# разделить формирование стрипов на видео и аудио, учитывать уже складированные стрипы, чтобы не занимали мест
# файл мета данных для автопостороения видеокадров
# вынести в интервйефс все вшитые параметры (номер трека, темп и тд)
# исправлять именования, сгруппировать интерфейс правильно
# вункционал ограничения построения трека до текущего положения кадра, опциональынм сделать